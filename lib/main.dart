import 'package:flutter/material.dart';
import 'package:wamf_onboarding/ui/playground/data/playground_datasource_impl.dart';
import 'package:wamf_onboarding/ui/playground/playground_view.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WAMF Onboarding',
      home: PlaygroundView(
        widgetList: PlaygroundDataSourceImpl().getList(),
      ),
    );
  }
}
