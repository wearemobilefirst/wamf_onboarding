import 'package:flutter/material.dart';
import 'package:wamf_onboarding/ui/molecules/molecule_example/mock_molecule_view_model.dart';
import 'package:wamf_onboarding/ui/molecules/molecule_example/molecule.dart';
import 'package:wamf_onboarding/ui/molecules/molecule_example/molecule_styles.dart';
import 'package:wamf_onboarding/ui/playground/data/playground_data_source.dart';
import 'package:wamf_onboarding/ui/playground/playground_view.dart';
import 'package:wamf_onboarding/ui/playground/playground_widget.dart';

class PlayGroundTasksDataSource implements PlaygroundDataSource {
  @override
  List<PlaygroundWidget> getList() {
    return [
      PlaygroundWidget(
        title: 'Molecule',
        child: PlaygroundView(
          widgetList: <Widget>[
            PlaygroundWidget(
              title: 'Molecule',
              child: Molecule(
                viewModel: MockMoleculeViewModel.buildRandom(),
                style: MoleculeStyles.buildStyle(),
              ),
            ),
            PlaygroundWidget(
              title: 'Molecule variant',
              child: Molecule(
                viewModel: MockMoleculeViewModel.buildRandom(),
                style: MoleculeStyles.buildStyleVariant(),
              ),
            ),
            PlaygroundWidget(
              title: 'Molecule variant 2',
              child: Molecule(
                viewModel: MockMoleculeViewModel.buildRandom(),
                style: MoleculeStyles.buildStyleVariantTwo(),
              ),
            ),
          ],
        ),
      ),
    ];
  }
}
