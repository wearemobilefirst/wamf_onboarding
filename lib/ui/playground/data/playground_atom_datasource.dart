import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:wamf_onboarding/ui/atoms/rounded_image.dart';
import 'package:wamf_onboarding/ui/playground/data/playground_data_source.dart';
import 'package:wamf_onboarding/ui/playground/playground_widget.dart';

class PlayGroundAtomDataSource implements PlaygroundDataSource {
  @override
  List<Widget> getList() {
    return [
      PlaygroundWidget(
        title: 'Rounded image',
        child: RoundedImage(
          image: AssetImage('assets/raw/mock_image.png'),
          imageHeight: 100,
          imageWidth: 100,
        ),
      ),
    ];
  }
}
