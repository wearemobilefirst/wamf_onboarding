import 'package:wamf_onboarding/ui/playground/data/playground_molecules_datasource.dart';
import 'package:wamf_onboarding/ui/playground/data/playground_atom_datasource.dart';
import 'package:wamf_onboarding/ui/playground/playground_view.dart';
import 'package:wamf_onboarding/ui/playground/playground_widget.dart';
import 'package:wamf_onboarding/ui/playground/data/playground_data_source.dart';

class PlaygroundDataSourceImpl implements PlaygroundDataSource {
  @override
  List<PlaygroundWidget> getList() {
    return [
          PlaygroundWidget(
            title: 'Atoms',
            child: PlaygroundView(
              widgetList: PlayGroundAtomDataSource().getList(),
            ),
          ),
        ] +
        PlayGroundTasksDataSource().getList();
  }
}
