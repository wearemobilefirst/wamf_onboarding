import 'package:flutter/widgets.dart';
import 'package:wamf_onboarding/utils/MockString.dart';

import 'molecule.dart';

class MockMoleculeViewModel {
  static MoleculeViewModel buildRandom() => MoleculeViewModel(
        title: _mockTitle.random(),
        subtitle: _mockSubtitle.random(),
        image: AssetImage('assets/raw/mock_image.png'),
      );
}

MockString _mockTitle = MockString(library: [
  'Title 1',
  'Title med 2',
  'Title medium 3',
  'Title large large large text 4',
]);

MockString _mockSubtitle = MockString(library: [
  'Subtitle 1',
  'Subtitle med 2',
  'Subtitle medium 3',
  'Subtitle large large large text 4',
]);
