import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:wamf_onboarding/ui/atoms/rounded_image.dart';

class MoleculeViewModel {
  String title;
  String subtitle;
  ImageProvider image;

  MoleculeViewModel({
    this.title,
    this.subtitle,
    this.image,
  });
}

class MoleculeStyle {
  final TextStyle titleStyle;
  final TextStyle subtitleStyle;
  final double imageSize;

  const MoleculeStyle({
    this.titleStyle,
    this.subtitleStyle,
    this.imageSize
  });

  MoleculeStyle copyWith({
    TextStyle titleStyle,
    TextStyle subtitleStyle,
    double imageSize,
  }) {
    return MoleculeStyle(
      titleStyle: titleStyle ?? this.titleStyle,
      subtitleStyle: subtitleStyle ?? this.subtitleStyle,
      imageSize: imageSize ?? this.imageSize
    );
  }
}

class _DefaultStyle extends MoleculeStyle {
  final TextStyle titleStyle =
      const TextStyle(fontSize: 20, fontWeight: FontWeight.bold);
  final TextStyle subtitleStyle =
      const TextStyle(fontSize: 14, fontWeight: FontWeight.w500);

  const _DefaultStyle({
    TextStyle titleStyle,
    TextStyle subtitleStyle,
  });
}

const MoleculeStyle _defaultStyle = const _DefaultStyle();

class Molecule extends StatelessWidget {
  final MoleculeStyle _style;
  final MoleculeViewModel _viewModel;

  Molecule({
    Key key,
    MoleculeViewModel viewModel,
    MoleculeStyle style = _defaultStyle,
  })  : this._viewModel = viewModel,
        this._style = style,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: RoundedImage(
        image: _viewModel.image,
        imageHeight: _style.imageSize,
        imageWidth: _style.imageSize,
      ),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(_viewModel.title, style: _style.titleStyle,),
          Text(_viewModel.subtitle, style: _style.subtitleStyle,),
        ],
      ),
    );
  }
}
