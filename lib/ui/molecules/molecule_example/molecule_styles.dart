import 'package:flutter/material.dart';

import 'molecule.dart';

class MoleculeStyles {
  static MoleculeStyle buildStyle() => _defaultMolecule;

  static MoleculeStyle buildStyleVariant() => _defaultMolecule.copyWith(
        imageSize: 50,
        subtitleStyle: TextStyle(
          fontSize: 13,
          fontWeight: FontWeight.w500,
        ),
      );

  static MoleculeStyle buildStyleVariantTwo() => _defaultMolecule.copyWith(
    imageSize: 150,
  );

  static const _defaultMolecule = MoleculeStyle(
    titleStyle: TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.bold,
    ),
    subtitleStyle: TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w500,
    ),
    imageSize: 100,
  );
}
