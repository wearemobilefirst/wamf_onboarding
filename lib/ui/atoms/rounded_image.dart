import 'package:flutter/widgets.dart';

class RoundedImage extends StatelessWidget {
  final ImageProvider image;
  final double imageHeight;
  final double imageWidth;
  final BorderRadiusGeometry imageBorderRadius;

  const RoundedImage({
    @required this.image,
    this.imageHeight = double.infinity,
    this.imageWidth = double.infinity,
    this.imageBorderRadius = const BorderRadius.all(Radius.circular(12.0)),
  });

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Container(
        width: imageWidth,
        height: imageHeight,
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: imageBorderRadius,
          image: DecorationImage(
            fit: BoxFit.cover,
            image: image,
          ),
        ),
      ),
    ]);
  }
}
