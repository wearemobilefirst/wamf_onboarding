# WAMF Onboarding

Base project made with Flutter with a Playground area to start working on.

# Working Methodology

## GIT

### GIT Flow

The branch strategy we use is [GIT Flow](http://www.wearemobilefirst.com/blog/how-to-use-the-full-power-of-source-control-and-agile-release-procedures/) for our projects, you can check the link to get more information of how to create new branches for your daily work.

### Commits

All the commits that we do, should be related to a Jira Task. We will identify the commit adding at the start of it [ID-XYZ] with a brief description. In the commit message we should explain briefly what we have done in the commit.

### Branch naming

The name of the branch has to include the Jira identifier too. We have 4 types of branch we can create, a part of the base ones:
- Feature, should be created as: feature/ID-XYZ-Short-Description 
- Bugfix, should be created as: bugfix/ID-XYZ-Short-Description
- Release, should be created as: release/X.Y.Z (version number)
- Hotfix, should be created as: hotfix/ID-XYZ-Short-Description

### Pull Request

- Minimum 2 reviewers to merge
- Use the same style for the PR names:
 `[ID-XYZ] Task solving issues`
- It’s mandatory to wait until CI finishes before merging to a branch
- Before merging the PR, we should solve all the comments that we have, and test that all is working fine in a device. If there’s a comment that we are disagree, we can discuss that and arrive to an agreement.
- We should test what we do before merging it to another branch
- We should use the PR template found at: [docs/pr_template.md](docs/pr_template.md)

## Working Practices

As we said before, we work with [Atomic design](http://bradfrost.com/blog/post/atomic-web-design) in the way to build our components and widgets for the app, this means that we would have:
- Atoms: Contains no other Atoms (only standard Flutter Widgets).
- Molecules: Are composed with one or more Atoms.
- Organisms: Are composed with one or more Molecules.
- Templates
- Pages

Every widget should have:
- New entry in the Playground area
- Class created using the live template given to build a Widget
- Business logic unit test

Also, if it's a Molecule or bigger than a Molecule, should have:
- [Golden test](https://medium.com/flutter-community/flutter-golden-tests-compare-widgets-with-snapshots-27f83f266cea) (screenshot test)

### When I should add a Style or a ViewModel to an item?

If you have an Atom, probably you wouldn't need to have a style or a ViewModel on it, to give to this Atom widget the style we want, we are going to send the style through attributes when we build this widget.

If you have something bigger than an Atom, it is, a Molecule, Organism, Template or Page, you would need to add a Style for it, and a ViewModel with the paramters it would be.

### Our way of working

We encourage you to read this [blog post](http://www.wearemobilefirst.com/blog/healthy-coding-practices) explaining the way we work

## No Global Scope

All the strings and constants should be inside a private class if not used outside the Widget. **We don't want Global Scope variables with no context.**
```
class _Strings {
    static const namePlaceholder = "Enter your name";
    static const surnamePlaceholder = "Enter your surname";
}
```

### Naming conventions

- Variables should not repeat the class name or context.

Do not:
```
class UserProfile {
    var userProfileName;
    var userProfileSubscriptions;
    var userProfileSkills;
}
```
Do:
```
class UserProfile {
    var name;
    var subscriptions;
    var skills;
}
```

- Variable names should not contain their type.

Do not:
```
var nameString;
var intAge;
var hasFinishedBoolean;
```
Do:
```
String name;
Int age;
bool hasFinished;
```
- Variable names should not contain a description of their current value.

Do not:
```
var blueBackgroundColor;
```
Do:

```
var backgroundColor;
```
- Variable names should not contain design specific

Do not:
```
var leftButton;
var rightButton;
```
Do:
```
var confirmButton;
var cancelButton;
```

## Live template

This Android Studio Live Template should be used for all Widgets except Atoms:


```dart
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


class $NAME$ViewModel {
  String example;

  $NAME$ViewModel({this.example});
}


class $NAME$Style {
  final $STYLECLASS$ $STYLENAME$;

  const $NAME$Style({this.$STYLENAME$});
  
  $NAME$Style copyWith(
      {$STYLECLASS$ $STYLENAME$,}){
     return $NAME$Style(
        $STYLENAME$: $STYLENAME$ ?? this.$STYLENAME$,
     );
      
  }
}

class _DefaultStyle extends $NAME$Style {
  final $STYLECLASS$ $STYLENAME$ = const $STYLECLASS$();

  const _DefaultStyle({
    $STYLECLASS$ $STYLENAME$,
  });

}

const $NAME$Style _defaultStyle = const _DefaultStyle();


class $NAME$ extends StatelessWidget {
  final $NAME$Style _style;
  final $NAME$ViewModel _viewModel;
  
    $NAME$(
      {Key key,
      $NAME$ViewModel viewModel,
      $NAME$Style style = _defaultStyle})
      : this._viewModel = viewModel,
        this._style = style,
        super(key: key);



  @override
  Widget build(BuildContext context) {
    return Container($END$);
  }
}
```

## Design and resources

Project design resources can be found on this [Zeplin link](https://app.zeplin.io/project/5c982aa6e6cbce24f564bc9f), we use Zeplin to show the app design and let you inspect the design to find the sizes and position of the items on it.

# Onboarding Tasks

You will have two days to work on these tasks, it's not mandatory to do all of them, you're free to choose from which one start and from which you want to continue.

You can make as much as you want, if you want to do one, is fine too.

All the work done should follow the guidelines provided and each Widget should have an entry in the Playground area with the name of the ticket in there and the name of the Widget created.

## Tasks 

We recommend you to use Mock data to try different sets of data and know that UI is working well with all the cases. This data can be mocked at ViewModel level or at Repository level, is up to you to decide where you would like to have the things mocked.

We tend to start with the smallest parts and mock them to get them working in the Playground, and then move to the more complicated compositions.

**The priority in these tasks is to get the UI working and is not essential to be fully working from the server feed.**

# WAM-1 - Sign Up

**Description**

If a user is not registered, would be able to register using the Sign Up form, writting the required fields.

**User story**

- As a user I have to write my name in the Sign Up form
- As a user I can write the company name (optional)
- As a user I have to write down my email in order to be able to log in lately
- As a user I have to write a strong password that matches the security criteria defined

**Acceptance Criteria**

- I am able to write in the form all the parameters defined.
- I am not able to continue if I don't have one of the required parameters.
- Visual feedback would be shown to inidcate why I am not able to continue
- Email field should be an email
- The password should be hidden until I press the "eye" icon that would show the password
- Sign Up button would be tappable when all the required fields are filled correctly
- Once Sign Up is done, a message to the user would be shown

**Design & resources**

https://app.zeplin.io/project/5c982aa6e6cbce24f564bc9f/screen/5c982af7ebdbb566f1199b08

**Notes**

Security criteria for password:
- Should have at least 8 characters
- Should include one letter and at least one number

Out of scope:
- Call the web service to register the user


# WAM-2 - Log In

**Description**

Within the Log In screen the user would be able to Log In in the app providing their user and password, if he does not know his password would be able to click on remmember my password option.

**User story**

- As a user I can write my Email
- As a user I can write my Password
- AS a user I can press the Log In button to Log In to the app

**Acceptance Criteria**

- I am able to write in the form all the parameters defined
- I am not able to continue if I did not fill all the parameters needed
- Visual feedback would be shown to inidcate why I am not able to continue
- The password should be hidden until I press the "eye" icon that would show the password.
- Log In button would be tappable when all the required fields are filled correctly
- When we Log In would appear a new screen saying that we had been Logged In succesfully.

**Design & resources**

https://app.zeplin.io/project/5c982aa6e6cbce24f564bc9f/screen/5c982af7ebdbb566f1199b4a

**Notes**

Out of scope:
- Forgot your password, just we should be able to click on it and show a message.
- Log In to a real server.

# WAM-3 - Report

**Description**

The Status report screen would show the reports of the tasks being done during a sprint, letting us know all the work done and topis and decisions taken during this time.

This report would come from a Trello board, and the information in the cards would be the one that is in the Trello cards.

**User story**
- As a user I am able to see the report cards
- As a user I am able to see the open topics cards
- As a user I can scroll horizontally through the cards
- As a user I can open a card to see the details

**Acceptance Criteria**

- I can see the cards, having the Jira ID as a link to the ticket.
- If I press the Jira link I would go to the website in a browser
- If I tap on a card I can see the information exapnded
- For the Tasks card, I can see 4 items on it
- For the Open topics card, I can see 4 lines of text
- If I tap on the arrow on the title of the section, I would go to a page where are all the items of this type

**Design & resources**

https://app.zeplin.io/project/5c982aa6e6cbce24f564bc9f/screen/5c982b1287814d603ef75cf6

**Notes**

\- -


# WAM-4 - Blog


**Description**

Our Blog section would show the blog posts we made so far as cards, and the user should be able to tap on them to open the detail.

**User story**

- As a user I am able to see the last blog posts made by WeAreMobileFirst
- As a user I can scroll down to see all the blogs made
- As a user I can tap on a card to see more details

**Acceptance Criteria**

- I can see all the blogs made
- I can tap on a card to see the detail

**Design & resources**

https://app.zeplin.io/project/5c982aa6e6cbce24f564bc9f/screen/5c982b12eb472f24e36c98b8 **

Feed URL: [http://www.wearemobilefirst.com/work/ ](http://www.wearemobilefirst.com/feed/json)

**Notes**

Out of scope:
- Detail screen
